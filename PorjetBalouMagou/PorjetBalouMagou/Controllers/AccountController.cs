﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PorjetBalouMagou.Controllers
{
    public class AccountController : Controller
    {
        private Classique_WebEntities db = new Classique_WebEntities();

        //
        // GET: /Account/

        public ActionResult Index()
        {
            var abonné = db.Abonné.Include(a => a.Pays);
            return View(abonné.ToList());
        }

        //
        // GET: /Account/Details/5

        public ActionResult Details(int id = 0)
        {
            Abonné abonné = db.Abonné.Find(id);
            if (abonné == null)
            {
                return HttpNotFound();
            }
            return View(abonné);
        }

        //
        // GET: /Account/Create

        public ActionResult Create()
        {
            ViewBag.Code_Pays = new SelectList(db.Pays, "Code_Pays", "Nom_Pays");
            return View();
        }

        //
        // POST: /Account/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Abonné abonné)
        {
            if (ModelState.IsValid)
            {
                db.Abonné.Add(abonné);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Code_Pays = new SelectList(db.Pays, "Code_Pays", "Nom_Pays", abonné.Code_Pays);
            return View(abonné);
        }

        //
        // GET: /Account/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Abonné abonné = db.Abonné.Find(id);
            if (abonné == null)
            {
                return HttpNotFound();
            }
            ViewBag.Code_Pays = new SelectList(db.Pays, "Code_Pays", "Nom_Pays", abonné.Code_Pays);
            return View(abonné);
        }

        //
        // POST: /Account/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Abonné abonné)
        {
            if (ModelState.IsValid)
            {
                db.Entry(abonné).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Code_Pays = new SelectList(db.Pays, "Code_Pays", "Nom_Pays", abonné.Code_Pays);
            return View(abonné);
        }

        //
        // GET: /Account/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Abonné abonné = db.Abonné.Find(id);
            if (abonné == null)
            {
                return HttpNotFound();
            }
            return View(abonné);
        }

        //
        // POST: /Account/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Abonné abonné = db.Abonné.Find(id);
            db.Abonné.Remove(abonné);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}