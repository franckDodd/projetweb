﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PorjetBalouMagou.Controllers
{
    public class AlbumController : Controller
    {
        private Classique_WebEntities db = new Classique_WebEntities();

        //
        // GET: /Album/

        public ActionResult Index(String initiale = null)
        {
            DbSqlQuery<Album> album = null;
            if (initiale != null)
                album = db.Album.SqlQuery("SELECT DISTINCT * FROM Album WHERE Album.Titre_Album LIKE '" + initiale + "%' ORDER BY Album.Titre_Album ;");
            else
                 album = db.Album.SqlQuery("SELECT DISTINCT * FROM Album;");
            return View(album.ToList());
        }
        //
        // GET: /Album/Details
        public ActionResult Details(int id = 0, int idMorceau = 0)
        {
            ViewData["IdMorceau"] = idMorceau;

            Album album = db.Album.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }

            var alb = db.Album.Find(id);
            ViewBag.nomAlbum = alb;

            DbSqlQuery<Enregistrement> enregistrement = db.Enregistrement.SqlQuery("SELECT Enregistrement.* FROM Enregistrement JOIN Composition_Disque ON Enregistrement.Code_Morceau = Composition_Disque.Code_Morceau JOIN Disque ON Composition_Disque.Code_Disque = Disque.Code_Disque JOIN Album ON Disque.Code_Album = Album.Code_Album WHERE Album.Code_Album = " + id + ";");
            ViewBag.Extrait = enregistrement.ToList();

            return View(enregistrement.ToList());
        }
        public ActionResult DetailsInterprete(int id = 0)
        {
            Musicien musicien = db.Musicien.Find(id);
            if (musicien == null)
            {
                return HttpNotFound();
            }

            DbSqlQuery<Album> albums = db.Album.SqlQuery("SELECT DISTINCT Album.* FROM Album JOIN Disque ON Album.Code_Album = Disque.Code_Album JOIN Composition_Disque ON Disque.Code_Disque = Composition_Disque.Code_Disque JOIN Enregistrement ON Composition_Disque.Code_Morceau = Enregistrement.Code_Morceau JOIN Interpréter ON Enregistrement.Code_Morceau = Interpréter.Code_Morceau JOIN Musicien ON Interpréter.Code_Musicien = Musicien.Code_Musicien WHERE Musicien.Code_Musicien = " + id + " ;");

            return View(albums.ToList());
        }

        public ActionResult DetailsCompositeurs(int id = 0)
        {
            Musicien musicien = db.Musicien.Find(id);
            if (musicien == null)
            {
                return HttpNotFound();
            }

            DbSqlQuery<Album> albums = db.Album.SqlQuery("SELECT DISTINCT Album.* FROM Album JOIN Disque ON Album.Code_Album = Disque.Code_Album JOIN Composition_Disque ON Disque.Code_Disque = Composition_Disque.Code_Disque JOIN Enregistrement ON Composition_Disque.Code_Morceau = Enregistrement.Code_Morceau JOIN Composition ON Enregistrement.Code_Composition = Composition.Code_Composition JOIN Composition_Oeuvre ON Composition.Code_Composition = Composition_Oeuvre.Code_Composition JOIN Oeuvre ON Composition_Oeuvre.Code_Oeuvre = Oeuvre.Code_Oeuvre JOIN Composer ON Oeuvre.Code_Oeuvre = Composer.Code_Oeuvre JOIN Musicien ON Composer.Code_Musicien = Musicien.Code_Musicien WHERE Musicien.Code_Musicien = " + id + ";");


            return View(albums.ToList());
        }
        public class BinaryResult : ActionResult
        {

            public string ContentType { get; set; }
            public byte[] Data { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                HttpResponseBase response = context.HttpContext.Response;
                response.ContentType = ContentType;

                if (Data != null)
                    response.BinaryWrite(Data);
            }
        }
        public ActionResult DetailsDirection(int id = 0)
        {
            Musicien musicien = db.Musicien.Find(id);
            if (musicien == null)
            {
                return HttpNotFound();
            }

            DbSqlQuery<Album> albums = db.Album.SqlQuery("SELECT DISTINCT Album.* FROM Album JOIN Disque ON Album.Code_Album = Disque.Code_Album JOIN Composition_Disque ON Disque.Code_Disque = Composition_Disque.Code_Disque JOIN Enregistrement ON Composition_Disque.Code_Morceau = Enregistrement.Code_Morceau JOIN Direction ON Enregistrement.Code_Morceau = Direction.Code_Morceau JOIN Musicien ON Direction.Code_Musicien = Musicien.Code_Musicien WHERE Musicien.Code_Musicien = " + id + ";");

            return View(albums.ToList());
        }
        public ActionResult Pochette(int id)
        {
            byte[] photo = db.Album.Where(i => i.Code_Album == id).Single().Pochette;

            return new BinaryResult() { ContentType = "image/jpeg", Data = photo };
        }
        public ActionResult Extrait(int id)
        {
            byte[] audio = null;
            try
            {
                audio = db.Enregistrement.Where(e => e.Code_Morceau == id).Single().Extrait;
            }
            catch (System.InvalidOperationException)
            {
                return null;
            }
            return new BinaryResult() { ContentType = "audio/mp3", Data = audio };
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}