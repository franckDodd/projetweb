﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PorjetBalouMagou.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Le bon groin";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Qui sommes-nous ?";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Nous contacter";

            return View();
        }
    }
}
