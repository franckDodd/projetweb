﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PorjetBalouMagou.Controllers
{
    public class MusicienController : Controller
    {
        private Classique_WebEntities db = new Classique_WebEntities();

        //
        // GET: /Musicien/

        public ActionResult Index()
        {
            DbSqlQuery<Musicien> musicien = null;
            musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* FROM Musicien ORDER BY Musicien.Nom_Musicien ;");

            return View(musicien.ToList());
        }
        public ActionResult ListeCompositeur(String initiale = null)
        {
            DbSqlQuery<Musicien> musicien = null;
            if (initiale != null)
                musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* FROM Musicien JOIN Composer ON Musicien.Code_Musicien = Composer.Code_Musicien  WHERE Musicien.Nom_Musicien LIKE '" + initiale + "%' ORDER BY Musicien.Code_Musicien ;");
            else
                musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* FROM Musicien JOIN Composer ON Musicien.Code_Musicien = Composer.Code_Musicien ORDER BY Musicien.Nom_Musicien ;");

            return View(musicien.ToList());
        }
        public ActionResult ListeInterprete(String initiale = null)
        {
            DbSqlQuery<Musicien> musicien = null;
            if (initiale != null)
                musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* FROM Musicien JOIN Interpréter ON Musicien.Code_Musicien = Interpréter.Code_Musicien  WHERE Musicien.Nom_Musicien LIKE '" + initiale + "%' ORDER BY Musicien.Code_Musicien ;");
            else
                musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* FROM Musicien JOIN Interpréter ON Musicien.Code_Musicien = Interpréter.Code_Musicien ORDER BY Musicien.Nom_Musicien ;");

            return View(musicien.ToList());
        }
        public ActionResult ListeDirection(String initiale = null)
        {
            DbSqlQuery<Musicien> musicien = null;
            if (initiale != null)
                musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* FROM Musicien JOIN Direction ON Direction.Code_Musicien = Direction.Code_Musicien  WHERE Musicien.Nom_Musicien LIKE '" + initiale + "%' ORDER BY Musicien.Code_Musicien ;");
            else
                musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* FROM Musicien JOIN Direction ON Musicien.Code_Musicien = Direction.Code_Musicien ORDER BY Musicien.Nom_Musicien ;");

            return View(musicien.ToList());
        }
        //
        // GET: /Musicien/Details/5

        public ActionResult Details(int id = 0)
        {
            Musicien musicien = db.Musicien.Find(id);
            if (musicien == null)
            {
                return HttpNotFound();
            }
            return View(musicien);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}